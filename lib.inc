section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        je .return
        inc rax
        jmp .loop
    .return:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    mov rdi, 1
    mov rax, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    mov rbx, 10
    mov r8, rsp
    dec rsp
    mov [rsp], byte 0
    mov rax, rdi

    .loop:
        xor rdx, rdx
        div rbx
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        test rax, rax
        jne .loop

    mov rdi, rsp
    call print_string
    mov rsp, r8
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .plus
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .plus:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:
    xor rcx, rcx
    .loop:
        mov al, [rdi + rcx]
        sub al, [rsi + rcx]
        cmp al, 0
        jne .return_false

        cmp byte [rdi + rcx], 0
        je .return_true

        inc rcx
        jmp .loop
    
    .return_false:
        xor rax, rax
        ret
    .return_true:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    dec rsp
    mov [rsp], byte 0
    mov rsi, rsp
    syscall

    mov al, [rsp]
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
  push    rsi
  push    rdi
  .while_space:
    call read_char
    cmp rax, ` `
    je .while_space
    cmp rax, `\t`
    je .while_space
    cmp rax, `\n`
    je .while_space
  pop     rdi
  pop     rsi

  xor rcx, rcx

  .loop:  
    cmp     rax, 0
    je      .return
    cmp     rax, ' '
    je      .return
    cmp     rax, `\t`
    je      .return
    cmp     rax, `\n`
    je      .return
    mov     [rdi + rcx], al
    
    push    rsi
    push    rdi
    push    rcx
    call    read_char
    pop     rcx
    pop     rdi
    pop     rsi

    inc     rcx
    cmp     rsi, rcx
    je      .big
    jne     .loop

  .big:
    xor     rax, rax
    ret

  .return:
    mov     [rdi + rcx], byte 0
    mov     rdx, rcx
    mov     rax, rdi
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
  push rbx
  mov r10, 10
  xor rax, rax
  xor rcx, rcx
  xor rbx, rbx
  xor rdx, rdx

.loop:
  mov bl, [rdi + rcx]
  cmp rbx, '0'
  jl .return
  cmp rbx, '9'
  jg .return
  sub rbx, '0'
  mul r10
  add rax, rbx
  inc rcx
  jmp .loop

.return:
  mov rdx, rcx
  pop rbx
  ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
  push rbx
  xor rbx, rbx
  mov bl, [rdi]
  cmp bl, '-'
  jne .positive
  inc rdi
.positive:
  call parse_uint
  cmp bl, '-'
  jne .return
  neg rax
  inc rdx

.return:
  pop rbx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  push rbx
  push rsi
  push rdi
  call string_length
  pop rdi
  pop rsi

  cmp rax, rdx
  jg .return_zero


  .loop:
    mov rbx, [rdi]
    mov [rsi], rbx
    cmp bl, 0
    je .return
    inc rdi
    inc rsi
    jmp .loop
    
  .return_zero:
    xor rax, rax
  .return:
    pop rbx
    ret
